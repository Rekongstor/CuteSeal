#pragma once
typedef unsigned long u64;
typedef unsigned int u32;
typedef unsigned short u16;
typedef int s32;
struct FILE;

namespace BMP2ASCII
{
    struct BMPHeader
    {
        u64 bType;
        u64 bSize;
        u64 bRevH;
        u64 bRevV;
        u64 bOffs;
    };

    struct BMPInfo
    {
        u32 bSize;
        s32 bWidth;
        s32 bHeight;
        u16 bPlanes;
        u16 bBitCnt;
        u32 bComr;
        u32 bImgSize;
        s32 bXPPM;
        s32 bYPPM;
        u32 bClrU;
        u32 bClrI;
    };

    struct RGBQuad
    {
        s32 bBlue;
        s32 bGreen;
        s32 bRed;
        s32 bRev;
    };

    FILE* f;

    int readbmp();
    char* bmp2ascii(char*);
}